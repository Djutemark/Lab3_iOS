//
//  ViewController.swift
//  Runkeeper
//
//  Created by Dennis Jutemark on 2017-11-18.
//  Copyright © 2017 Dennis Jutemark. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import AVFoundation

class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    var distance: Double = 0
    
    // Map related
    var locationManager: CLLocationManager!
    
    // Sound related
    var timesPlayedApplause = 0
    var audioPlayer: AVAudioPlayer?
    
    // Time related
    var startTime: Double = 0
    var time: Double = 0
    weak var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        distanceLabel.text = "You've run: \(distance) m"
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest

        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.mapType = MKMapType(rawValue: 0)!
        mapView.userTrackingMode = MKUserTrackingMode(rawValue: 2)!

        let status = CLLocationManager.authorizationStatus()
        if status == .notDetermined || status == .denied || status == .authorizedWhenInUse {
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
        }
        locationManager.startUpdatingLocation()
        locationManager.startUpdatingHeading()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        startTime = Date().timeIntervalSinceReferenceDate
        timer = Timer.scheduledTimer(timeInterval: 0.05,
                                     target: self,
                                     selector: #selector(advanceTimer(timer:)),
                                     userInfo: nil,
                                     repeats: true)
    }
    
    @objc func advanceTimer(timer: Timer) {
        time = Date().timeIntervalSinceReferenceDate - startTime
        let (h,m,s) = secondsToHMS(seconds: Int(time))
        timeLabel.text = ("\(h):\(m):\(s)")
    }
    func secondsToHMS ( seconds: Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }

    // Variables used in locationManager but must not reset after every function call
    var previousPosition = CLLocation(), currentPosition = CLLocation()
    var firstCheck = true, secondCheck = true
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // These 2 checks are for correctly calculating the distance when simulating a device, probably redundant irl
        if firstCheck {
            firstCheck = false
            return
        } else if secondCheck {
            currentPosition = locations.first!
            secondCheck = false
            return
        }
        previousPosition = currentPosition
        currentPosition = locations.first!
        
        distance += currentPosition.distance(from: previousPosition)
        distanceLabel.text = "You've run: \(distance) m"
        
        if timesPlayedApplause + 1 <= Int(distance) / 1000 {
            timesPlayedApplause += 1
            
            do {
                let sound = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "applause4", ofType: "mp3")!))
                audioPlayer = sound
                audioPlayer?.numberOfLoops = 1
                audioPlayer?.prepareToPlay()
                audioPlayer?.play()
            } catch {
                print("error playing sound")
            }
            
        }
        
        var area = [CLLocationCoordinate2D(latitude: currentPosition.coordinate.latitude, longitude: currentPosition.coordinate.longitude),
                    CLLocationCoordinate2D(latitude: previousPosition.coordinate.latitude, longitude: previousPosition.coordinate.longitude)]
        let polyline = MKPolyline(coordinates: &area, count: area.count)
        mapView.add(polyline)
    }

    func mapView(_ mapView: MKMapView!, rendererFor overlay: MKOverlay!) -> MKOverlayRenderer! {
        if overlay is MKPolyline {
            let pr = MKPolylineRenderer(overlay: overlay)
            pr.strokeColor = UIColor.red
            pr.lineWidth = 2
            return pr
        }
        return nil
    }

}

